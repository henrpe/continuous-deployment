# Continuous deployment

Continuous deployment is a strategy for software releases wherein any code commit that passes the automated testing phase is automatically released into the production environment, making changes that are visible to the software's users.

## Git
* there are three kinds of branches: master, feature/ and hotfix/ (optional)
  * master branch is always ready to be released. it should be released automatically under normal circumstances
    * if it is not released automatically then hotfix/ branches should be used for patching critical production bugs
  * feature/ branches are meant for normal development. they exists for a short period of time and will be merged to master when feature development is complete
    * each Jira issue is developed in a separate feature/ branch
    * feature/ branch should be merged to master when
      * branch build succeeds (static code analysis, tests, build/compile)
      * all merge request discussions / problems have been resolved
      * code reviewer approves pull request
      * branch has been manually tested
  * hotfix/ branch is **optional** and only needed if master branch is not continuously released to production
    * hotfix/ branch should be created from the tag/commit currently deployed to production environment
    * hotfix/ branch can be deployed to staging/production and merged to master when: 
      * branch build succeeds (static code analysis, tests, build/compile)
      * all merge request discussions / problems have been resolved
      * code reviewer approves pull request
      * branch has been manually tested
      
## Jenkins
* automatically detects and builds all branches
  * Multibranch Pipeline ("creates a set of Pipeline projects according to detected branches in one SCM repository") should be used as project type to keep branches separate.
  * build process should perform static code analysis, tests, build/compile etc
    * if any of these steps fail then the whole build should fail
  * if a master branch succeeds then it should automatically be deployed to staging
  * if staging deploy succeeds then it should automatically be deployed to production
  
## Code 
* at least 90% code/branch test coverage should be enforced
  * JavaScript: `jest` dependency
  * Java: `jacoco` dependency
* clean code principles should be used
  
## Production monitoring
* production environment should automatically be monitored and a notification should be sent to slack or mailing list when a pre-configured error threshold is exceeded